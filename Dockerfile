FROM centos:latest
MAINTAINER mwasilewski

RUN yum -y update
RUN yum -y install wget

RUN wget http://storage.googleapis.com/kubernetes-helm/helm-v2.0.2-linux-amd64.tar.gz
RUN tar -zxvf ./helm-v2.0.2-linux-amd64.tar.gz
RUN mv linux-amd64/helm /usr/local/bin/helm

RUN yum clean all